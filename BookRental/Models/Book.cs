﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BookRental.Models
{
    public class Book
    {
        [Required]
        public int BookID { get; set; }
        [Required]
        private string ISBN;
        [Required]
        public string Title { get; set; }
        [Required]
        public string Author { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.ImageUrl)]
        public string ImageUrl { get; set; }
        [Required]
        [Range(0, 1000)]
        public int Availability { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        public double Price { get; set; }

        [DisplayFormat(DataFormatString = "{0:MMM dd yyyyy}")]
        public DateTime? DateAdded { get; set; }

        [Required]
        public int GenreId { get; set; }

        public Genre Genre { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM dd yyyyy}")]
        public DateTime PublificationDate { get; set; }

        [Required]
        public int Pages { get; set; }
        [Required]
        public string ProductDimension { get; set; }
    }
}